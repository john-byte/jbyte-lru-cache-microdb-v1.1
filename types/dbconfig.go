package types

import trie "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/trie"

type TDbRawConfig struct {
	WrittenPageId uint32 `json:"writtenPageId"`
	PageCapacity  uint32 `json:"pageCapacity"`
	Pages         string `json:"pages"`
}

type TDbConfig struct {
	WrittenPageId uint32
	PageCapacity  uint32
	Pages         *trie.Trie
}
