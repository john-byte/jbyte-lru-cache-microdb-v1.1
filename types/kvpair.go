package types

type KVPair struct {
	Key interface{}
	Val interface{}
}
