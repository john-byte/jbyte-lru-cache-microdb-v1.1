package microdb

import (
	"fmt"
	"gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/types"
	errorsutils "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/errors"
	fileutils "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/file"
	lrucache "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/lru_cache"
	orderedmap "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/ordered_map"
	trie "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/trie"
	"log"
	"path"
	"sync"
)

type TMicroDb struct {
	index           *trie.Trie
	indexLock       *sync.Mutex
	hotCache        *lrucache.LruCache
	reservedPage    *orderedmap.OrderedMap
	writtenPageLock *sync.Mutex
	writtenPage     *orderedmap.OrderedMap
	writtenPageId   uint32
	pageCapacity    uint32
	rootPath        string
}

func New(rootPath string, pageCapacity uint32) *TMicroDb {
	index := trie.New()
	hotCache := lrucache.New(uint64(pageCapacity))
	reservedPage := orderedmap.New()
	writtenPage := orderedmap.New()
	return &TMicroDb{
		index:           &index,
		indexLock:       &sync.Mutex{},
		hotCache:        hotCache,
		reservedPage:    &reservedPage,
		writtenPageLock: &sync.Mutex{},
		writtenPage:     &writtenPage,
		writtenPageId:   0,
		pageCapacity:    pageCapacity,
		rootPath:        rootPath,
	}
}

func readConfig(rootPath string) (*types.TDbConfig, error) {
	configPath := path.Join(rootPath, "index.json")
	configFile, err := fileutils.ReadFileWTimeout(
		configPath,
		6000,
	)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"readConfig",
			err,
		)
	}
	parsedConfig, err := parseConfig(configFile)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"readConfig",
			err,
		)
	}

	return parsedConfig, nil
}

func readPage(rootPath string, pageId uint32) (*orderedmap.OrderedMap, error) {
	pagePath := path.Join(
		rootPath,
		fmt.Sprintf("page-%v.dat", pageId),
	)
	pageFile, err := fileutils.ReadFileWTimeout(
		pagePath,
		6000,
	)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"readHottestPage",
			err,
		)
	}
	parsedPage, err := parsePage(pageFile)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"readHottestPage",
			err,
		)
	}

	return parsedPage, nil
}

func FromFile(rootPath string) (*TMicroDb, error) {
	config, err := readConfig(rootPath)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"FromFile",
			err,
		)
	}

	hottestPageId := 1
	if config.WrittenPageId > 1 {
		hottestPageId = int(config.WrittenPageId) - 1
	}
	hottestPage, err := readPage(rootPath, uint32(hottestPageId))
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"FromFile",
			err,
		)
	}

	writtenPage, err := readPage(rootPath, uint32(config.WrittenPageId))
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"FromFile",
			err,
		)
	}

	hotCache := lrucache.FromOrderedMap(
		hottestPage,
		uint64(config.PageCapacity),
	)

	reservedPage := orderedmap.New()

	return &TMicroDb{
		index:           config.Pages,
		indexLock:       &sync.Mutex{},
		hotCache:        hotCache,
		reservedPage:    &reservedPage,
		writtenPageLock: &sync.Mutex{},
		writtenPage:     writtenPage,
		writtenPageId:   config.WrittenPageId,
		pageCapacity:    config.PageCapacity,
		rootPath:        rootPath,
	}, nil
}

func (self *TMicroDb) flushWrittenPage(
	key string,
	val string,
	currPageId uint32,
	currPage *orderedmap.OrderedMap,
) {
	go func() {
		self.writtenPageLock.Lock()
		defer self.writtenPageLock.Unlock()

		pageFilePath := path.Join(
			self.rootPath,
			fmt.Sprintf("page-%v.dat", currPageId),
		)
		pageBytes := serializePage(currPage)
		err := fileutils.WriteFileWTimeout(
			pageFilePath,
			pageBytes,
			0666,
			6000,
		)
		if err != nil {
			formatedErr := errorsutils.LogError(
				"MicroDb",
				"flushWrittenPage",
				err,
			)
			errStr := fmt.Sprintf(
				"%v",
				formatedErr.Error(),
			)
			log.Println(errStr)
		}
	}()
}

func (self *TMicroDb) flushIndex(key string, currPageId uint32) {
	go func() {
		self.indexLock.Lock()
		defer self.indexLock.Unlock()

		indexFilePath := path.Join(
			self.rootPath,
			"index.json",
		)
		indexBytes, err := serializeConfig(&types.TDbConfig{
			WrittenPageId: currPageId,
			PageCapacity:  self.pageCapacity,
			Pages:         self.index,
		})
		if err != nil {
			goto errorPoint
		}

		err = fileutils.WriteFileWTimeout(
			indexFilePath,
			indexBytes,
			0666,
			6000,
		)
		if err != nil {
			goto errorPoint
		} else {
			return
		}

	errorPoint:
		{
			formatedErr := errorsutils.LogError(
				"MicroDb",
				"flushIndex",
				err,
			)
			errStr := fmt.Sprintf(
				"%v",
				formatedErr.Error(),
			)
			log.Println(errStr)
		}
	}()
}

func (self *TMicroDb) putInHotCache(key string, val string) {
	self.hotCache.Put(key, val)

	self.indexLock.Lock()
	_, entryInIndex := self.index.Get([]rune(key))
	self.indexLock.Unlock()
	if !entryInIndex {
		currPageId := self.writtenPageId

		self.writtenPageLock.Lock()
		self.writtenPage.Put(key, val)
		currPage := self.writtenPage
		if currPage.Len() == uint64(self.pageCapacity) {
			newPage := orderedmap.New()
			self.writtenPage = &newPage
			self.writtenPageId++
		}
		self.writtenPageLock.Unlock()
		self.flushWrittenPage(key, val, currPageId, currPage)

		self.indexLock.Lock()
		self.index.Put([]rune(key), currPageId)
		self.indexLock.Unlock()
		self.flushIndex(key, currPageId)
	}
}

func (self *TMicroDb) Get(key string) (string, error) {
	val, hasInHotCache := self.hotCache.Get(key)
	if hasInHotCache {
		return val.(string), nil
	}

	val, hasInReservedPage := self.reservedPage.Get(key)
	if hasInReservedPage {
		self.putInHotCache(key, val.(string))
		return val.(string), nil
	}

	self.indexLock.Lock()
	rawPageId, hasPage := self.index.Get([]rune(key))
	self.indexLock.Unlock()
	if !hasPage {
		return "", nil
	}
	pageFilePath := path.Join(
		self.rootPath,
		fmt.Sprintf("page-%v.dat", rawPageId.(uint32)),
	)
	pageFile, err := fileutils.ReadFileWTimeout(
		pageFilePath,
		6000,
	)
	if err != nil {
		return "", errorsutils.LogError(
			"MicroDb",
			"Get",
			err,
		)
	}
	reservedPage, err := parsePage(pageFile)
	if err != nil {
		return "", errorsutils.LogError(
			"MicroDb",
			"Get",
			err,
		)
	}
	self.reservedPage = reservedPage
	val, _ = self.reservedPage.Get(key)
	self.putInHotCache(key, val.(string))

	return val.(string), nil
}

func (self *TMicroDb) Put(key string, val string) {
	self.putInHotCache(key, val)
}
