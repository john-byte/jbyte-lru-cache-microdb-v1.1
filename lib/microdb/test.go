package microdb

import (
	"fmt"
	errorsutils "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/errors"
	"io/ioutil"
	"path"
	"time"
)

func TestSerDeserOfConfig() {
	inputPath := path.Join("db", "test-serdeser-config", "input-index.json")
	inputFile, err := ioutil.ReadFile(inputPath)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestSerDeserOfConfig",
			err,
		)
		panic(formatedErr)
	}

	parsedIndex, err := parseConfig(inputFile)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestSerDeserOfConfig",
			err,
		)
		panic(formatedErr)
	}

	serializedIndex, err := serializeConfig(parsedIndex)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestSerDeserOfConfig",
			err,
		)
		panic(formatedErr)
	}

	outputPath := path.Join("db", "test-serdeser-config", "output-index.json")
	err = ioutil.WriteFile(
		outputPath,
		serializedIndex,
		0666,
	)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestSerDeserOfConfig",
			err,
		)
		panic(formatedErr)
	}
}

func TestSerDeserOfPage() {
	inputPath := path.Join("db", "test-serdeser-page", "input-page-1.dat")
	inputFile, err := ioutil.ReadFile(inputPath)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestSerDeserOfPage",
			err,
		)
		panic(formatedErr)
	}

	parsedPage, err := parsePage(inputFile)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestSerDeserOfPage",
			err,
		)
		panic(formatedErr)
	}

	serializedPage := serializePage(parsedPage)
	outputPath := path.Join("db", "test-serdeser-page", "output-page-1.dat")
	err = ioutil.WriteFile(
		outputPath,
		serializedPage,
		0666,
	)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestSerDeserOfPage",
			err,
		)
		panic(formatedErr)
	}
}

func TestHottestCache() {
	rootPath := path.Join("db", "test-hottest-cache-flow")

	microDb, err := FromFile(rootPath)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}

	microDb.Put("felixargyle11", "xxx")
	microDb.Put("felixargylexxx", "yyy")
	microDb.Put("fanton111", "777")
	microDb.Put("fant888", "1000")

	timeout := time.NewTimer(time.Second * 8)
	<-timeout.C

	val1, err := microDb.Get("felixargyle11")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for felixargyle11 => %v\n", val1)

	val2, err := microDb.Get("felixargylexxx")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for felixargylexxx => %v\n", val2)

	val3, err := microDb.Get("fanton111")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for fanton111 => %v\n", val3)

	val4, err := microDb.Get("fant888")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for fant888 => %v\n", val4)
}

func TestReservePage() {
	rootPath := path.Join("db", "test-reserve-page-flow")

	microDb, err := FromFile(rootPath)
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestReservePage",
			err,
		)
		panic(formatedErr)
	}

	// Old batch
	microDb.Put("felixargyle11", "xxx")
	microDb.Put("felixargylexxx", "yyy")
	microDb.Put("fanton111", "777")
	microDb.Put("fant888", "1000")

	// New batch
	microDb.Put("felix", "wewex")
	microDb.Put("apple", "rrrr")
	microDb.Put("aplon", "1121")
	microDb.Put("yyy", "2332dsff")

	timeout := time.NewTimer(time.Second * 14)
	<-timeout.C

	// New batch
	val1, err := microDb.Get("felix")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for felix => %v\n", val1)

	val2, err := microDb.Get("apple")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for apple => %v\n", val2)

	val3, err := microDb.Get("aplon")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for aplon => %v\n", val3)

	val4, err := microDb.Get("yyy")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for yyy => %v\n", val4)

	// Old batch
	val5, err := microDb.Get("felixargyle11")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for felixargyle11 => %v\n", val5)

	val6, err := microDb.Get("felixargylexxx")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for felixargylexxx => %v\n", val6)

	val7, err := microDb.Get("fanton111")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for fanton111 => %v\n", val7)

	val8, err := microDb.Get("fant888")
	if err != nil {
		formatedErr := errorsutils.LogError(
			"DbIndex",
			"TestHottestCache",
			err,
		)
		panic(formatedErr)
	}
	fmt.Printf("value for fant888 => %v\n", val8)
}
