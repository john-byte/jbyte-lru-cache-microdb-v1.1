package microdb

import (
	"bytes"
	"encoding/json"
	"fmt"
	types "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/types"
	errorsutils "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/errors"
	orderedmap "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/ordered_map"
	"gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/trie"
	"strconv"
	"strings"
)

func serializePage(page *orderedmap.OrderedMap) []byte {
	res := bytes.Buffer{}

	pageIter := page.GetIterator()
	for pageIter.HasNext() {
		keyVal := pageIter.Next()
		insertedStr := fmt.Sprintf(
			"%v:%v\n",
			keyVal.Key.(string),
			keyVal.Val.(string),
		)
		res.WriteString(insertedStr)
	}

	return res.Bytes()
}

func parsePage(raw []byte) (*orderedmap.OrderedMap, error) {
	res := orderedmap.New()

	ptr := 0
	for ptr < len(raw) {
		keyBuff := bytes.Buffer{}
		valBuff := bytes.Buffer{}

		for ptr < len(raw) && raw[ptr] != ':' {
			keyBuff.WriteByte(raw[ptr])
			ptr++
		}
		if ptr == len(raw) {
			return nil, errorsutils.LogError(
				"MicroDb",
				"parseReservedPage",
				fmt.Errorf(
					"Missing colon at %v",
					ptr,
				),
			)
		}
		ptr++

		for ptr < len(raw) && raw[ptr] != '\n' {
			valBuff.WriteByte(raw[ptr])
			ptr++
		}
		ptr++

		key := keyBuff.String()
		val := valBuff.String()
		res.Put(key, val)
	}

	return &res, nil
}

func serializeIndex(node *trie.TrieNode) string {
	res := strings.Builder{}

	char := node.Character
	if node.Character == '#' || node.Character == '^' {
		char = '$'
	}
	var val uint32 = 0
	if node.Value != nil {
		val = node.Value.(uint32)
	}

	nodeStr := fmt.Sprintf("%s:%v,", string(char), val)
	res.WriteString(nodeStr)

	for _, ch := range node.Children {
		childStr := serializeIndex(ch)
		res.WriteString(childStr)
	}

	if node.Parent != nil {
		if node.IsEndOfWord {
			res.WriteString("#,")
		} else {
			res.WriteString("^,")
		}
	}
	return res.String()
}

func serializeConfig(config *types.TDbConfig) ([]byte, error) {
	indexStr := serializeIndex(config.Pages.Root)

	obj := types.TDbRawConfig{
		Pages:         indexStr,
		WrittenPageId: config.WrittenPageId,
		PageCapacity:  config.PageCapacity,
	}
	result, err := json.Marshal(obj)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"serializeConfig",
			err,
		)
	}

	return result, nil
}

func parseIndex(raw string) (*trie.Trie, error) {
	rawIter := []rune(raw)
	var currNode *trie.TrieNode = nil
	ptr := 0

	for ptr < len(rawIter) {
		if rawIter[ptr] == '#' {
			if currNode == nil {
				return nil, errorsutils.LogError(
					"MicroDb",
					"deserializeIndex",
					fmt.Errorf("Undexpected # at %v", ptr),
				)
			}

			currNode.IsEndOfWord = true
			currNode = currNode.Parent
			ptr += 2
			continue
		}

		if rawIter[ptr] == '^' {
			if currNode == nil {
				return nil, errorsutils.LogError(
					"MicroDb",
					"deserializeIndex",
					fmt.Errorf("Undexpected ^ at %v", ptr),
				)
			}

			currNode = currNode.Parent
			ptr += 2
			continue
		}

		char := rawIter[ptr]
		ptr++
		if ptr == len(rawIter) || rawIter[ptr] != ':' {
			return nil, errorsutils.LogError(
				"MicroDb",
				"deserializeIndex",
				fmt.Errorf(
					"Missing colon at %v",
					ptr,
				),
			)
		}
		ptr++

		pageNumBuff := strings.Builder{}
		for ptr < len(rawIter) && ('0' <= rawIter[ptr] && rawIter[ptr] <= '9') {
			pageNumBuff.WriteRune(rawIter[ptr])
			ptr++
		}
		if ptr < len(rawIter) && rawIter[ptr] != ',' {
			return nil, errorsutils.LogError(
				"MicroDb",
				"deserializeIndex",
				fmt.Errorf(
					"Missing comma at %v",
					ptr,
				),
			)
		}
		ptr++

		newNode := trie.MakeNode()
		newNode.Character = char
		pageNum, _ := strconv.Atoi(pageNumBuff.String())
		newNode.Value = uint32(pageNum)
		newNode.Parent = currNode

		if currNode == nil {
			currNode = newNode
		} else {
			currNode.Children[char] = newNode
			currNode = currNode.Children[char]
		}
	}

	return trie.FromRoot(currNode), nil
}

func parseConfig(raw []byte) (*types.TDbConfig, error) {
	var rawConfig *types.TDbRawConfig
	err := json.Unmarshal(raw, &rawConfig)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"deserializeConfig",
			err,
		)
	}

	index, err := parseIndex(rawConfig.Pages)
	if err != nil {
		return nil, errorsutils.LogError(
			"MicroDb",
			"deserializeConfig",
			err,
		)
	}

	res := types.TDbConfig{
		WrittenPageId: rawConfig.WrittenPageId,
		PageCapacity:  rawConfig.PageCapacity,
		Pages:         index,
	}

	return &res, nil
}
