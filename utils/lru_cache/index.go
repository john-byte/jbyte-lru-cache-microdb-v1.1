package lrucache

import (
	"gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/types"
	orderedmap "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/ordered_map"
)

type LruCache struct {
	entries  *orderedmap.OrderedMap
	capacity uint64
	length   uint64
}

func New(capacity uint64) *LruCache {
	ordMap := orderedmap.New()
	return &LruCache{
		entries:  &ordMap,
		capacity: capacity,
		length:   0,
	}
}

func FromOrderedMap(ordMap *orderedmap.OrderedMap, capacity uint64) *LruCache {
	return &LruCache{
		entries:  ordMap,
		capacity: capacity,
		length:   ordMap.Len(),
	}
}

func FromKVPairs(pairs []types.KVPair, capacity uint64) *LruCache {
	entries := orderedmap.New()

	for _, pair := range pairs {
		entries.Put(pair.Key, pair.Val)
	}

	return &LruCache{
		entries:  &entries,
		capacity: capacity,
		length:   uint64(len(pairs)),
	}
}

func (self *LruCache) propagateEntry(key interface{}, val interface{}) {
	self.entries.Delete(key)
	self.entries.Put(key, val)
}

func (self *LruCache) Get(key interface{}) (interface{}, bool) {
	val, hasKey := self.entries.Get(key)
	if !hasKey {
		return val, hasKey
	}

	self.propagateEntry(key, val)
	return val, true
}

func (self *LruCache) Put(key interface{}, val interface{}) *types.KVPair {
	_, hadKey := self.entries.Get(key)
	self.propagateEntry(key, val)
	if hadKey {
		return nil
	}

	self.length++
	if self.length > self.capacity {
		begin := self.entries.BeginKV()
		self.entries.Delete(begin.Key)
		self.length--

		return &begin
	}

	return nil
}

func (self *LruCache) Delete(key interface{}) (interface{}, bool) {
	val, hadKey := self.entries.Delete(key)
	if hadKey {
		self.length--
	}
	return val, hadKey
}
