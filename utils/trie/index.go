package trie

type TrieNode struct {
	Parent      *TrieNode
	Children    map[rune]*TrieNode
	Character   rune
	Value       interface{}
	IsEndOfWord bool
}

type Trie struct {
	Root *TrieNode
}

func New() Trie {
	root := TrieNode{
		Parent:      nil,
		Children:    make(map[rune]*TrieNode, 0),
		Character:   '#',
		Value:       nil,
		IsEndOfWord: false,
	}

	return Trie{
		Root: &root,
	}
}

func MakeNode() *TrieNode {
	return &TrieNode{
		Parent:      nil,
		Children:    make(map[rune]*TrieNode, 0),
		Character:   '#',
		Value:       nil,
		IsEndOfWord: false,
	}
}

func FromRoot(root *TrieNode) *Trie {
	res := Trie{
		Root: root,
	}
	return &res
}

func (self *Trie) Put(key []rune, val interface{}) {
	node := self.Root

	for _, ch := range key {
		if _, hasChild := node.Children[ch]; !hasChild {
			node.Children[ch] = &TrieNode{
				Parent:      node,
				Character:   ch,
				Children:    make(map[rune]*TrieNode, 0),
				IsEndOfWord: false,
			}
		}

		node = node.Children[ch]
	}

	node.IsEndOfWord = true
	node.Value = val
}

func (self *Trie) Get(key []rune) (interface{}, bool) {
	node := self.Root

	for _, ch := range key {
		if _, hasChild := node.Children[ch]; !hasChild {
			return nil, false
		}

		node = node.Children[ch]
	}

	if !node.IsEndOfWord {
		return nil, false
	}

	return node.Value, true
}

type TrieKV struct {
	Key []rune
	Val interface{}
}

type TrieIterator struct {
	stack []*TrieNode
}

func (trie *Trie) GetIterator() *TrieIterator {
	return &TrieIterator{
		stack: []*TrieNode{trie.Root},
	}
}

func (self *TrieIterator) HasNext() bool {
	return len(self.stack) > 0
}

func (self *TrieIterator) Next() *TrieKV {
	for len(self.stack) > 0 {
		node := self.stack[len(self.stack)-1]
		self.stack = self.stack[:len(self.stack)-1]

		for _, ch := range node.Children {
			self.stack = append(self.stack, ch)
		}

		if node.IsEndOfWord {
			word := backtrackWord(node)
			return &TrieKV{
				Key: word,
				Val: node.Value,
			}
		}
	}

	return nil
}
