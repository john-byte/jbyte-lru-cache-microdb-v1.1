package trie

func backtrackWord(node *TrieNode) []rune {
	word := make([]rune, 0)

	for node.Parent != nil {
		word = append(word, node.Character)
		node = node.Parent
	}

	l := 0
	r := len(word) - 1
	for l < r {
		temp := word[l]
		word[l] = word[r]
		word[r] = temp
		l++
		r--
	}

	return word
}
