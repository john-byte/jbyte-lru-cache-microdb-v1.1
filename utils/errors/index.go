package errorsutils

import (
	"fmt"
	timeutils "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/time"
)

func LogError(
	moduleName string,
	methodName string,
	err error,
) error {
	return fmt.Errorf(
		"[%v] [%v / %v] => %v",
		timeutils.NowTimestamp(),
		moduleName,
		methodName,
		err.Error(),
	)
}
