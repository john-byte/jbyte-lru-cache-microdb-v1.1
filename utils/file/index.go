package fileutils

import (
	"errors"
	"io/ioutil"
	"os"
	"time"
)

func ReadFileWTimeout(fileName string, timeoutMs int) ([]byte, error) {
	resChan := make(chan []byte)
	errChan := make(chan error)
	timer := time.NewTimer(time.Millisecond * time.Duration(timeoutMs))

	go func() {
		bytes, err := ioutil.ReadFile(fileName)
		if err != nil {
			errChan <- err
		} else {
			resChan <- bytes
		}
	}()

	select {
	case <-timer.C:
		{
			close(resChan)
			close(errChan)
			return nil, errors.New("File read timeout")
		}
	case bytes := <-resChan:
		{
			close(resChan)
			close(errChan)
			return bytes, nil
		}
	case err := <-errChan:
		{
			close(resChan)
			close(errChan)
			return nil, err
		}
	}
}

func WriteFileWTimeout(fileName string, data []byte, perm int, timeoutMs int) error {
	errChan := make(chan error)
	timer := time.NewTimer(time.Millisecond * time.Duration(timeoutMs))

	go func() {
		err := ioutil.WriteFile(fileName, data, os.FileMode(perm))
		errChan <- err
	}()

	select {
	case <-timer.C:
		{
			close(errChan)
			return errors.New("File write timeout")
		}
	case err := <-errChan:
		{
			close(errChan)
			return err
		}
	}
}
