package orderedmap

import (
	types "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/types"
	linkedlist "gitlab.com/john-byte/jbyte-lru-cache-microdb-v1.1/utils/linked_list"
)

type OrderedMap struct {
	dict      map[interface{}]interface{}
	nodesDict map[interface{}]*linkedlist.Node
	list      linkedlist.List
	length    uint64
}

type OrderedMapIterator struct {
	currNode *linkedlist.Node
}

func New() OrderedMap {
	res := OrderedMap{
		dict:      make(map[interface{}]interface{}),
		nodesDict: make(map[interface{}]*linkedlist.Node),
		list:      linkedlist.New(),
		length:    0,
	}

	return res
}

func FromMap(rawMap map[interface{}]interface{}) OrderedMap {
	dict := make(map[interface{}]interface{})
	nodesDict := make(map[interface{}]*linkedlist.Node)
	list := linkedlist.New()
	var length uint64 = 0

	for k, v := range rawMap {
		dict[k] = v
		list.AppendVal(types.KVPair{
			Key: k,
			Val: v,
		})
		nodesDict[k] = list.Tail()
		length++
	}

	return OrderedMap{
		dict:      dict,
		nodesDict: nodesDict,
		list:      list,
		length:    length,
	}
}

func (self *OrderedMap) Keys() []interface{} {
	res := make([]interface{}, 0)
	iter := self.getIteratorImpl()
	for iter.HasNext() {
		res = append(res, iter.Next().Key)
	}
	return res
}

func (self *OrderedMap) Values() []interface{} {
	res := make([]interface{}, 0)
	iter := self.getIteratorImpl()
	for iter.HasNext() {
		res = append(res, iter.Next().Val)
	}
	return res
}

func (self *OrderedMap) Get(key interface{}) (interface{}, bool) {
	val, hasKey := self.dict[key]
	return val, hasKey
}

func (self *OrderedMap) Put(key interface{}, val interface{}) {
	_, hadKey := self.dict[key]
	self.dict[key] = val

	if hadKey {
		self.nodesDict[key].Val = types.KVPair{
			Key: key,
			Val: val,
		}
		return
	}

	self.list.AppendVal(types.KVPair{
		Key: key,
		Val: val,
	})
	self.nodesDict[key] = self.list.Tail()
	self.length++
}

func (self *OrderedMap) Delete(key interface{}) (interface{}, bool) {
	_, hasKey := self.dict[key]
	if !hasKey {
		return nil, false
	}

	delete(self.dict, key)
	node := self.nodesDict[key]
	self.list.RemoveNode(node)
	delete(self.nodesDict, key)
	self.length--

	return node.Val, true
}

func (self *OrderedMap) getIteratorImpl() OrderedMapIterator {
	return OrderedMapIterator{
		currNode: self.list.Head(),
	}
}

func (self *OrderedMap) GetIterator() OrderedMapIterator {
	return self.getIteratorImpl()
}

func (self *OrderedMap) BeginKV() types.KVPair {
	return self.list.Head().Val.(types.KVPair)
}

func (self *OrderedMap) EndKV() types.KVPair {
	return self.list.Tail().Val.(types.KVPair)
}

func (self *OrderedMap) Len() uint64 {
	return self.length
}

func (self *OrderedMapIterator) HasNext() bool {
	return self.currNode != nil
}

func (self *OrderedMapIterator) Next() types.KVPair {
	res := self.currNode.Val
	self.currNode = self.currNode.Next
	return res.(types.KVPair)
}
